﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class LifeSpanHandler : CefLifeSpanHandler
	{
		private WebView view;

		public LifeSpanHandler(WebView view)
		{
			this.view = view;
		}

		protected override void OnAfterCreated(CefBrowser browser)
		{
			base.OnAfterCreated(browser);

			this.view.OnCreated(browser);
		}
	}
}
