﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class CefBootstrapper
	{
		private static bool isInitialized;

		public static bool IsInitialized
		{
			get
			{
				return isInitialized;
			}
		}

		public static void Initialize(CefSettings settings)
		{
			//var initialization = Task.Factory.StartNew(CefRuntime.Load);

			CefRuntime.Load();

			//initialization.ContinueWith(t =>
			//{
			if (IsInitialized)
			{
				return;
			}

			var mainArgs = new CefMainArgs(new string[] { });
			var app = new WebApp();

			isInitialized = SafeInitializeCefRuntime(mainArgs, settings, app);



			//}, TaskScheduler.FromCurrentSynchronizationContext());

			//return initialization;
		}

		public static void Terminate()
		{
			CefRuntime.QuitMessageLoop();
		}

		private static bool SafeInitializeCefRuntime(CefMainArgs mainArgs, CefSettings settings, CefApp app)
		{
			var initialized = false;

			if (Debugger.IsAttached)
			{
				CrashReporting.HookCrashReporterWithMono(() => initialized = Initialize(mainArgs, settings, app));

			}
			else
			{
				initialized = Initialize(mainArgs, settings, app);
			}

			return initialized;
		}

		private static bool Initialize(CefMainArgs mainArgs, CefSettings settings, CefApp app)
		{
			try
			{
				CefRuntime.Initialize(mainArgs, settings, app, windowsSandboxInfo: IntPtr.Zero);
			}
			catch (InvalidOperationException)
			{
				return false;
			}

			return true;
		}
	}
}
