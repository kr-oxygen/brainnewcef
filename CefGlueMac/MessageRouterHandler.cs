﻿using System;
using Xilium.CefGlue;
using Xilium.CefGlue.Wrapper;

namespace BrainNewCef
{
	public class MessageRouterHandler : CefMessageRouterBrowserSide.Handler
	{
		private WebView webView;

		public MessageRouterHandler(WebView webView)
		{
			this.webView = webView;
		}

		public override bool OnQuery(CefBrowser browser, CefFrame frame, long queryId, string request, bool persistent, CefMessageRouterBrowserSide.Callback callback)
		{
			//if (request == "wait5")
			//{
			//	new Thread(() =>
			//	{
			//		Thread.Sleep(5000);
			//		callback.Success("success! responded after 5 sec timeout."); // TODO: at this place crash can occurs, if application closed
			//	}).Start();
			//	return true;
			//}

			//if (request == "wait5f")
			//{
			//	new Thread(() =>
			//	{
			//		Thread.Sleep(5000);
			//		callback.Failure(12345, "success! responded after 5 sec timeout. responded as failure.");
			//	}).Start();
			//	return true;
			//}

			//if (request == "wait30")
			//{
			//	new Thread(() =>
			//	{
			//		Thread.Sleep(30000);
			//		callback.Success("success! responded after 30 sec timeout.");
			//	}).Start();
			//	return true;
			//}

			//if (request == "noanswer")
			//{
			//	return true;
			//}

			//var chars = request.ToCharArray();
			//Array.Reverse(chars);
			//var response = new string(chars);
			//callback.Success(response);

			var splitRequest = request.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
			var objectName = splitRequest[0];
			var methodName = splitRequest[1];
			object result;
			Type resultType;
			string exception;

			this.webView.BindingHandler.ExecuteBoundMethod(browser, objectName, methodName, null, out result, out resultType, out exception);

			callback.Success(result?.ToString());

			return true;
		}

		public override void OnQueryCanceled(CefBrowser browser, CefFrame frame, long queryId)
		{
		}
	}
}
