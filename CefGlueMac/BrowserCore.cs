﻿using System;
using System.Collections.Generic;

namespace BrainNewCef
{
	public class BrowserCore
	{
		private static readonly IDictionary<string, object> boundObjects = new Dictionary<string, object>();

		internal static IDictionary<string, object> BoundObjects
		{
			get
			{
				return boundObjects;
			}
		}

		internal static void RegisterJsObject(string name, object objectToBind)
		{
			boundObjects[name] = objectToBind;
		}
	}
}
