﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class ResourceHandler : CefResourceHandler
	{
		private readonly WebView view;
		private readonly IDictionary<string, HttpResponseMessage> responseMessageByUrl;
		private ResponseWrapper currentResponse;
		private Queue<ResponseWrapper> pendingResponses;
		private Queue<CefCallback> pendingCallbacks;

		private object resourceHandlerLock = new object();

		public ResourceHandler(WebView view)
		{
			this.view = view;

			this.responseMessageByUrl = new Dictionary<string, HttpResponseMessage>();
			this.pendingResponses = new Queue<ResponseWrapper>();
			this.pendingCallbacks = new Queue<CefCallback>();
		}

		internal bool TryProcessRequest(CefRequest request)
		{
			HttpResponseMessage response;

			if (this.view.TryProcessRequest(request.Url, out response))
			{
				this.responseMessageByUrl[request.Url] = response;

				return true;
			}

			return false;
		}

		protected override bool ProcessRequest(CefRequest request, CefCallback callback)
		{
			lock (resourceHandlerLock)
			{
				HttpResponseMessage response;

				if (this.responseMessageByUrl.TryGetValue(request.Url, out response))
				{
					var headers = new Dictionary<string, string>();
					foreach (var header in response.Headers)
					{
						headers.Add(header.Key, header.Value.FirstOrDefault());
					}

					var content = response.Content;
					var wrapper = new ResponseWrapper(content.Headers.ContentType.MediaType,
						response.ReasonPhrase, response.StatusCode, content.ReadAsStreamAsync().Result, headers);

					if (this.currentResponse == null)
					{
						this.currentResponse = wrapper;

						callback.Continue();
					}
					else
					{
						this.pendingResponses.Enqueue(wrapper);
						this.pendingCallbacks.Enqueue(callback);
					}

					this.responseMessageByUrl.Remove(request.Url);

					return true;
				}

				return false;
			}
		}

		protected override void GetResponseHeaders(CefResponse response, out long responseLength, out string redirectUrl)
		{
			lock (resourceHandlerLock)
			{
				redirectUrl = null;

				var lastResponse = this.currentResponse;

				response.MimeType = lastResponse.MimeType;
				response.StatusText = lastResponse.StatusText;
				response.Status = lastResponse.Status;
				response.SetHeaderMap(lastResponse.Headers);

				responseLength = lastResponse.Stream.Length;

				if (responseLength == 0)
				{
					// When the responseLength == 0 the ReadResponse handler is not called, so we ProcessPendingResponses here instead
					this.ProcessPendingResponses();
				}
			}
		}

		protected override bool ReadResponse(System.IO.Stream response, int bytesToRead, out int bytesRead, CefCallback callback)
		{
			lock (resourceHandlerLock)
			{
				var bytes = new byte[bytesToRead];
				this.currentResponse.Stream.Read(bytes, 0, bytesToRead);
				response.Write(bytes, 0, bytesToRead);

				if (this.currentResponse.Stream.Position >= this.currentResponse.Stream.Length)
				{
					this.ProcessPendingResponses();
				}

				bytesRead = bytesToRead;

				return true;
			}
		}

		protected override bool CanGetCookie(CefCookie cookie)
		{
			return true;
		}

		protected override bool CanSetCookie(CefCookie cookie)
		{
			return true;
		}

		protected override void Cancel()
		{

		}

		private void ProcessPendingResponses()
		{
			this.currentResponse.Dispose();
			this.currentResponse = null;

			if (this.pendingCallbacks.Count != 0 && this.pendingResponses.Count != 0)
			{
				this.currentResponse = this.pendingResponses.Dequeue();
				var callback = this.pendingCallbacks.Dequeue();
				callback.Continue();
			}
		}
	}
}
