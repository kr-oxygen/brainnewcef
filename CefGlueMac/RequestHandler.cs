﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class RequestHandler : CefRequestHandler
	{
		private WebView webView;
		private ResourceHandler handler;

		public RequestHandler(WebView webView)
		{
			this.webView = webView;
			this.handler = new ResourceHandler(this.webView);
		}

		protected override CefReturnValue OnBeforeResourceLoad(CefBrowser browser, CefFrame frame, CefRequest request, CefRequestCallback callback)
		{
			this.webView.RaiseBeforeResourceLoad(request);

			return base.OnBeforeResourceLoad(browser, frame, request, callback);
		}

		protected override CefResourceHandler GetResourceHandler(CefBrowser browser, CefFrame frame, CefRequest request)
		{
			if (this.handler.TryProcessRequest(request))
			{
				return this.handler;
			}

			return null;
		}
	}
}
