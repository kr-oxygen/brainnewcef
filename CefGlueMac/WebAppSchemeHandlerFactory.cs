﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class WebAppSchemeHandlerFactory : CefSchemeHandlerFactory
	{
		private readonly WebView webView;

		public WebAppSchemeHandlerFactory(WebView webView)
		{
			this.webView = webView;
		}

		protected override CefResourceHandler Create(CefBrowser browser, CefFrame frame, string schemeName, CefRequest request)
		{
			return new ResourceHandler(this.webView);
		}
	}
}
