﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class LoadHandler : CefLoadHandler
	{
		private WebView webView;

		public LoadHandler(WebView webView)
		{
			this.webView = webView;
		}

		protected override void OnLoadStart(CefBrowser browser, CefFrame frame, CefTransitionType transitionType)
		{
			this.webView.OnLoadStart(frame.IsMain, frame.Url);
		}

		protected override void OnLoadEnd(CefBrowser browser, CefFrame frame, int httpStatusCode)
		{
			this.webView.OnLoadEnd(frame.IsMain, frame.Url);
		}
	}
}
