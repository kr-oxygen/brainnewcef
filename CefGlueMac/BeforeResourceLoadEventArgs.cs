﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class BeforeResourceLoadEventArgs : EventArgs
	{
		private CefRequest request { get; set; }

		public BeforeResourceLoadEventArgs(CefRequest request)
		{
			this.request = request;
		}

		public void AddHeader(string headerName, string value)
		{
			var headers = this.request.GetHeaderMap();
			headers[headerName] = value;
			this.request.SetHeaderMap(headers);
		}
	}
}
