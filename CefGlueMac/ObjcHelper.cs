﻿using System;
using System.Runtime.InteropServices;
using ObjCRuntime;

namespace BrainNewCef
{
	public static class ObjcHelper
	{
		[DllImport("/usr/lib/libobjc.dylib")]
		static extern IntPtr objc_getProtocol(string name);

		[DllImport("/usr/lib/libobjc.dylib")]
		static extern bool class_addProtocol(IntPtr cls, IntPtr protocol);

		public static IntPtr GetProtocol(string name)
		{
			return objc_getProtocol(name);
		}

		public static bool AddProtocol(this Class cls, IntPtr protocol)
		{
			return class_addProtocol(cls.Handle, protocol);
		}
	}
}
