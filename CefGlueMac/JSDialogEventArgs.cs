﻿using System;
namespace BrainNewCef
{
	public class JSDialogEventArgs : EventArgs
	{
		public string Message { get; set; }

		public string DefaultValue { get; set; }

		public string UserInput { get; set; }

		public bool Handled { get; set; }

		public bool ReturnValue { get; set; }

		public JSDialogEventArgs(string message)
			: this(message, string.Empty)
		{

		}

		public JSDialogEventArgs(string message, string defaultValue)
		{
			this.Message = message;
			this.DefaultValue = defaultValue;
		}
	}
}
