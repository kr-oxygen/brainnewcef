﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;

namespace BrainNewCef
{
	public class ResponseWrapper : IDisposable
	{
		private readonly IDictionary<string, string> headers;

		public string MimeType { get; private set; }
		public string StatusText { get; private set; }

		public int Status { get; private set; }

		public Stream Stream { get; private set; }

		public NameValueCollection Headers
		{
			get
			{
				var nameValueCollection = new NameValueCollection();
				foreach (var header in this.headers)
				{
					nameValueCollection.Set(header.Key, header.Value);
				}

				return nameValueCollection;
			}
		}

		public ResponseWrapper(string mimeType, string statusText, HttpStatusCode statusCode, Stream stream, IDictionary<string, string> headers)
		{
			this.MimeType = mimeType;
			this.StatusText = statusText;
			this.Status = (int)statusCode;
			this.headers = headers;
			this.Stream = stream;
		}

		public void Dispose()
		{
			if (this.Stream != Stream.Null)
			{
				this.Stream.Dispose();
			}
		}
	}
}
