﻿using System;
namespace BrainNewCef
{
	public static class Constants
	{
		public static readonly string EvaluateJavaScriptSemaphoreName = "EvaluateJavaScriptSemaphore";
		public static readonly string ContextCreatedMessageName = "ClientRenderer.ContextCreated";
		public static readonly string ContextReleasedMessageName = "ClientRenderer.ContextReleased";
		public static readonly string ExecuteBoundMethodMessageName = "ClientRenderer.ExecuteBoundMethod";
		public static readonly string ObjectsToBindMessageName = "WebBrowser.ObjectsToBind";

		public static readonly string LocalSchemeName = "client";
		public static readonly string FileSchemeName = "file";
	}
}
