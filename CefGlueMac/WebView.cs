﻿using System;
using AppKit;
using Xilium.CefGlue;
using Xilium.CefGlue.Wrapper;
using CoreGraphics;
using System.Net.Http;
using System.Net.Http.Headers;

namespace BrainNewCef
{
	public class WebView : NSView
	{
		private readonly string startUrl;

		protected readonly CefClient client;
		protected readonly CefBrowserSettings settings;

		private CefBrowser browser;
		private double initialZoomLevel = 0d;
		private bool shouldShowDevTools;
		private bool loadHandlerRequested;

		private NSCursor cursor;

		internal BindingHandler BindingHandler { get; private set; }

		public static CefMessageRouterBrowserSide BrowserMessageRouter { get; private set; }

		public event EventHandler<LoadEventArgs> LoadStart;
		public event EventHandler<LoadEventArgs> LoadEnd;
		public event EventHandler<DevToolsShownEventArgs> DevToolsShown;
		public event EventHandler<BeforeResourceLoadEventArgs> BeforeResourceLoad;

		public event EventHandler Created;

		public bool IsBrowserInitialized
		{
			get
			{
				return this.browser != null;
			}
		}

		public bool IsMouseCursorChangeDisabled
		{
			get
			{
				return this.IsBrowserInitialized ? this.browser.GetHost().IsMouseCursorChangeDisabled : false;
			}
			set
			{
				var browserHost = this.browser.GetHost();
				if (browserHost.IsMouseCursorChangeDisabled != value)
				{
					browserHost.SetMouseCursorChangeDisabled(value);
				}
			}
		}

		public WebView(string startUrl, CefBrowserSettings browserSettings)
					: this(browserSettings)
		{
			this.startUrl = startUrl;
		}

		protected WebView(CefBrowserSettings browserSettings)
		{
			this.AutoresizingMask = NSViewResizingMask.WidthSizable
			| NSViewResizingMask.HeightSizable
			| NSViewResizingMask.MinXMargin
			| NSViewResizingMask.MinYMargin
			| NSViewResizingMask.MaxXMargin
			| NSViewResizingMask.MaxYMargin;

			this.settings = browserSettings;
			this.BindingHandler = new BindingHandler();
			this.client = new WebClient(this, this.BindingHandler);

			//this.RegisterMessageRouter();

			//CefRuntime.RegisterSchemeHandlerFactory(Constants.LocalSchemeName, string.Empty, new WebAppSchemeHandlerFactory(this));
		}

		public void SetScaleFactor(double scale)
		{
			// NOTE: As WebKit zoom levels are not very straight-forward we want to convert them to scale factor
			// Conversion between scale factor and zoom level is inffered from these conversations
			// https://code.google.com/p/chromium/issues/detail?id=71484 and http://www.magpcss.org/ceforum/viewtopic.php?f=6&t=11491
			var zoomLevel = Math.Log(scale, 1.2);
			this.SetZoomLevel(zoomLevel);
		}

		public double GetScaleFactor()
		{
			var zoomLevel = this.GetZoomLevel();
			return Math.Pow(1.2, zoomLevel);
		}

		public void SetZoomLevel(double zoomLevel)
		{
			if (this.IsBrowserInitialized)
			{
				this.browser.GetHost().SetZoomLevel(zoomLevel);
			}
			else
			{
				this.initialZoomLevel = zoomLevel;
			}
		}

		public double GetZoomLevel()
		{
			return this.IsBrowserInitialized ? this.browser.GetHost().GetZoomLevel() : this.initialZoomLevel;
		}

		public override void ViewDidMoveToSuperview()
		{
			base.ViewDidMoveToSuperview();
			if (this.Superview != null)
			{
				this.CreateBrowser(this.Frame.Size);
			}
		}

		public override void ResetCursorRects()
		{
			base.ResetCursorRects();

			if (this.cursor != null)
			{
				this.AddCursorRect(this.Bounds, this.cursor);
				this.cursor.SetOnMouseEntered(true);
			}
		}

		public void ActivateCursor(NSCursor cursor)
		{
			this.IsMouseCursorChangeDisabled = true;

			this.cursor = cursor;
		}

		public void Navigate(string location)
		{
			this.browser.StopLoad();
			this.browser.GetMainFrame().LoadUrl(location);
		}

		public void ShowDevTools()
		{
			if (this.IsBrowserInitialized && this.loadHandlerRequested)
			{
				this.ShowDevToolsCore();
			}
			else
			{
				this.shouldShowDevTools = true;
			}
		}

		public void CloseDevTools()
		{
			var parentHost = this.browser.GetHost();
			parentHost.CloseDevTools();
		}

		public void Reload()
		{
			this.browser.ReloadIgnoreCache();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.ClearBrowser();
			}

			base.Dispose(disposing);
		}

		internal void OnCreated(CefBrowser newBrowser)
		{
			this.browser = newBrowser;

			this.RaiseCreated();

			this.SetZoomLevel(this.initialZoomLevel);
		}

		internal void LoadHandlerRequested()
		{
			this.loadHandlerRequested = true;
			if (this.shouldShowDevTools)
			{
				this.ShowDevToolsCore();
			}
		}

		internal void OnLoadStart(bool isMain, string url)
		{
			this.SetInitialZoom();

			this.RaiseLoadEvent(this.LoadStart, isMain, url);
		}

		internal void OnLoadEnd(bool isMain, string url)
		{
			this.RaiseLoadEvent(this.LoadEnd, isMain, url);
		}

		private void ShowDevToolsCore()
		{
			this.shouldShowDevTools = false;

			CefBrowserSettings settings = new CefBrowserSettings();

			var devToolsWebView = new DevToolsView(settings, this.browser);

			this.RaiseDevToolsShown(devToolsWebView);
		}

		private void ClearBrowser()
		{
			if (this.browser != null)
			{
				this.browser.Dispose();
				this.browser = null;
			}
		}

		private void CreateBrowser(CGSize viewSize)
		{
			if (this.IsBrowserInitialized)
			{
				throw new InvalidOperationException("The browser is already created.");
			}

			var windowInfo = GetWindowInfo(viewSize, this.Handle);

			CefBrowserHost.CreateBrowser(windowInfo, this.client, this.settings, this.startUrl);
		}

		private void RaiseLoadEvent(EventHandler<LoadEventArgs> loadEvent, bool isMain, string url)
		{
			var handler = loadEvent;
			if (handler != null)
			{
				handler(this, new LoadEventArgs(isMain, url));
			}
		}

		private void RaiseCreated()
		{
			var handler = this.Created;
			if (handler != null)
			{
				handler(this, EventArgs.Empty);
			}
		}

		internal void RaiseBeforeResourceLoad(CefRequest request)
		{
			var handler = this.BeforeResourceLoad;
			if (handler != null)
			{
				handler(this, new BeforeResourceLoadEventArgs(request));
			}
		}

		private void RaiseDevToolsShown(WebView devToolsView)
		{
			var handler = this.DevToolsShown;
			if (handler != null)
			{
				handler(this, new DevToolsShownEventArgs(devToolsView));
			}
		}

		private void RegisterMessageRouter()
		{
			if (!CefRuntime.CurrentlyOn(CefThreadId.UI))
			{
				PostTask(CefThreadId.UI, this.RegisterMessageRouter);
				return;
			}

			// window.cefQuery({ request: 'my_request', onSuccess: function(response) { console.log(response); }, onFailure: function(err,msg) { console.log(err, msg); } });
			WebView.BrowserMessageRouter = new CefMessageRouterBrowserSide(new CefMessageRouterConfig());
			WebView.BrowserMessageRouter.AddHandler(new MessageRouterHandler(this));
		}

		public static void PostTask(CefThreadId threadId, Action action)
		{
			CefRuntime.PostTask(threadId, new ActionTask(action));
		}

		internal sealed class ActionTask : CefTask
		{
			public Action _action;

			public ActionTask(Action action)
			{
				_action = action;
			}

			protected override void Execute()
			{
				_action();
				_action = null;
			}
		}

		public delegate void Action();

		private void SetInitialZoom()
		{
			if (this.initialZoomLevel != 0)
			{
				var currentZoom = this.GetZoomLevel();
				if (currentZoom != this.initialZoomLevel)
				{
					this.SetZoomLevel(this.initialZoomLevel);
				}

				this.initialZoomLevel = 0;
			}
		}

		protected static CefWindowInfo GetWindowInfo(CGSize size, IntPtr frameHandle)
		{
			var windowInfo = CefWindowInfo.Create();
			windowInfo.X = 0;
			windowInfo.Y = 0;
			windowInfo.Width = (int)size.Width;
			windowInfo.Height = (int)size.Height;

			windowInfo.ParentHandle = frameHandle;

			return windowInfo;
		}

		public void RegisterJsObject(string name, object objectToBind)
		{
			BrowserCore.RegisterJsObject(name, objectToBind);
		}

		public void ExecuteScript(string script)
		{
			browser.GetMainFrame().ExecuteJavaScript(script, "about:blank", 0);
		}

		public event EventHandler<JSDialogEventArgs> JSAlert;
		public event EventHandler<JSDialogEventArgs> JSConfirm;
		public event EventHandler<JSDialogEventArgs> JSPrompt;

		internal bool OnAlert(string message)
		{
			var args = new JSDialogEventArgs(message);

			this.RaiseDialogEvent(args, this.JSAlert);

			return args.Handled;
		}

		internal bool OnConfirm(string message, out bool returnValue)
		{
			var args = new JSDialogEventArgs(message);

			this.RaiseDialogEvent(args, this.JSConfirm);

			returnValue = args.ReturnValue;

			return args.Handled;
		}

		internal bool OnPrompt(string message, string defaultValue, out bool returnValue, out string userInput)
		{
			var args = new JSDialogEventArgs(message, defaultValue);

			this.RaiseDialogEvent(args, this.JSPrompt);

			returnValue = args.ReturnValue;
			userInput = args.UserInput;

			return args.Handled;
		}

		private void RaiseDialogEvent(JSDialogEventArgs args, EventHandler<JSDialogEventArgs> handler)
		{
			if (handler != null)
			{
				handler(this, args);
			}
		}

		public event EventHandler<RequestResourceEventArgs> RequestResource;

		internal bool TryProcessRequest(string url, out HttpResponseMessage response)
		{
			response = new HttpResponseMessage();

			var args = new RequestResourceEventArgs(url);

			this.RaiseProcessRequest(args);

			if (args.CanProcess)
			{
				response.Content = new StreamContent(args.Stream);
				response.Content.Headers.ContentType = new MediaTypeHeaderValue(args.MimeType);
				response.ReasonPhrase = args.StatusText;
				response.StatusCode = args.StatusCode;

				foreach (var header in args.Headers)
				{
					response.Headers.Add(header.Key, header.Value);
				}
			}

			return args.CanProcess;
		}

		private void RaiseProcessRequest(RequestResourceEventArgs args)
		{
			var handler = this.RequestResource;
			if (handler != null)
			{
				handler(this, args);
			}
		}
	}
}
