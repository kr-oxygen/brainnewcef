﻿using System;
namespace BrainNewCef
{
	public class DevToolsShownEventArgs : EventArgs
	{
		private readonly WebView webView;

		public DevToolsShownEventArgs(WebView webView)
		{
			this.webView = webView;
		}

		public WebView View
		{
			get { return this.webView; }
		}
	}
}
