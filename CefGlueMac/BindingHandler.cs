﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class BindingHandler
	{
		internal void ContextCreated(CefBrowser browser)
		{
			foreach (var item in BrowserCore.BoundObjects)
			{
				var processMessage = CefProcessMessage.Create(Constants.ObjectsToBindMessageName);
				var methods = item.Value.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public).Select(method => method.Name).ToArray();
				var cefListValue = CefListValue.Create();

				for (int i = 0; i < methods.Count(); i++)
				{
					cefListValue.SetString(i, methods[i]);
				}

				processMessage.Arguments.SetString(0, item.Key);
				processMessage.Arguments.SetList(1, cefListValue);

				browser.SendProcessMessage(CefProcessId.Renderer, processMessage);
			}
		}

		internal void ExecuteBoundMethod(CefBrowser browser, string objectName, string methodName, CefListValue argumentsList, out object result, out Type resultType, out string exception)
		{
			exception = string.Empty;
			result = null;
			resultType = null;

			object boundObject;
			if (BrowserCore.BoundObjects.TryGetValue(objectName, out boundObject))
			{
				var type = boundObject.GetType();

				var members = type.GetMember(methodName, MemberTypes.Method, BindingFlags.Instance | BindingFlags.Public);

				if (!members.Any())
				{
					exception = string.Format("No member named {0} found.", methodName);

					return;
				}

				IList<object> convertedArguments = new List<object>();
				if (argumentsList != null)
				{
					convertedArguments = ConvertArguments(argumentsList);
				}

				MethodInfo method;
				IList<object> methodArguments;
				FindBestMethod(convertedArguments, members, out method, out methodArguments);

				if (method != null)
				{
					try
					{
						result = method.Invoke(boundObject, methodArguments.ToArray());
						resultType = method.ReturnType;
					}
					catch (TargetInvocationException err)
					{
						exception = err.InnerException.Message;
					}
					catch (Exception err)
					{
						exception = err.Message;
					}
				}
				else
				{
					exception = "Argument mismatch for method \"" + methodName + "\".";
				}
			}
		}

		static IList<object> ConvertArguments(CefListValue argumentsList)
		{
			IList<object> convertedArguments = new List<object>();

			for (int i = 0; i < argumentsList.Count; i++)
			{
				var valueType = argumentsList.GetValueType(i);
				switch (valueType)
				{
					case CefValueType.Bool:
						convertedArguments.Add(argumentsList.GetBool(i));
						break;
					case CefValueType.Double:
						convertedArguments.Add(argumentsList.GetDouble(i));
						break;
					case CefValueType.Int:
						convertedArguments.Add(argumentsList.GetInt(i));
						break;
					case CefValueType.List:
						convertedArguments.Add(argumentsList.GetList(i));
						break;
					case CefValueType.String:
						convertedArguments.Add(argumentsList.GetString(i));
						break;
					case CefValueType.Null:
						convertedArguments.Add(null);
						break;
				}
			}
			return convertedArguments;
		}

		private static void FindBestMethod(IList<object> suppliedArguments, IEnumerable<MemberInfo> members, out MethodInfo bestMethod, out IList<object> bestMethodArguments)
		{
			// choose best method
			bestMethod = null;
			bestMethodArguments = null;
			int bestMethodCost = -1;

			foreach (var method in members.OfType<MethodInfo>())
			{
				var parametersInfo = method.GetParameters();

				if (suppliedArguments.Count == parametersInfo.Length)
				{
					int failed = 0;
					int cost = 0;
					var arguments = new List<Object>();

					try
					{
						for (int i = 0; i < suppliedArguments.Count; i++)
						{
							var paramType = parametersInfo[i].ParameterType;
							int paramCost = GetChangeTypeCost(suppliedArguments[i], paramType);

							if (paramCost < 0)
							{
								failed++;
								break;
							}
							else
							{
								arguments.Add(ChangeType(suppliedArguments[i], paramType));
								cost += paramCost;
							}
						}
					}
					catch (Exception)
					{
						failed++;
					}

					if (failed > 0)
					{
						continue;
					}

					if (cost < bestMethodCost || bestMethodCost < 0)
					{
						bestMethod = method;
						bestMethodArguments = arguments;
						bestMethodCost = cost;
					}

					// this is best as possible cost
					if (cost == 0)
						break;
				}
			}
		}

		private static object ChangeType(Object value, Type conversionType)
		{
			if (GetChangeTypeCost(value, conversionType) < 0)
			{
				throw new Exception("No conversion available.");
			}

			if (value == null) return null;

			var targetType = Nullable.GetUnderlyingType(conversionType);
			if (targetType != null) conversionType = targetType;

			return Convert.ChangeType(value, conversionType);
		}

		private static int GetChangeTypeCost(object value, Type conversionType)
		{
			// TODO: temporary Int64 support fully disabled
			if (conversionType == typeof(Int64) || conversionType == typeof(Nullable<Int64>)
				|| conversionType == typeof(UInt64) || conversionType == typeof(Nullable<UInt64>))
			{
				return -1;
			}

			// Null conversion
			if (value == null)
			{
				// TODO: This is check for reference type, may be not accuracy.
				if (!conversionType.IsValueType)
				{
					return 0;
				}

				// Nullable types also can be converted to null without penalty.
				if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
				{
					return 0;
				}

				// Non-reference and non-nullable types can not be converted from null.
				return -1;
			}

			// value is not null

			// value have same type - no conversion required
			var valueType = value.GetType();
			if (valueType == conversionType)
			{
				return 0;
			}

			int baseCost = 0;

			// but conversionType can be Nullable
			var targetType = Nullable.GetUnderlyingType(conversionType);
			if (targetType != null)
			{
				// this is a nullable type, and it cost + 1
				conversionType = targetType;
				baseCost++;
			}

			if (valueType == conversionType)
			{
				return baseCost + 0;
			}

			if (valueType == typeof(Boolean))
			{
				// Boolean can be converted only to Boolean
				if (conversionType == typeof(Boolean)) return baseCost + 0;
				return -1;
			}
			else if (valueType == typeof(Int32))
			{
				int int32Val = (int)value;

				if (conversionType == typeof(Int32)) return baseCost + 0;
				else if (conversionType == typeof(UInt32) && (int32Val >= 0)) return baseCost + 1;
				else if (conversionType == typeof(Int16) && (int32Val >= Int16.MinValue && int32Val <= Int16.MaxValue)) return baseCost + 2;
				else if (conversionType == typeof(UInt16) && (int32Val >= UInt16.MinValue && int32Val <= UInt16.MaxValue)) return baseCost + 3;
				else if (conversionType == typeof(Char) && (int32Val >= Char.MinValue && int32Val <= Char.MaxValue)) return baseCost + 4;
				else if (conversionType == typeof(SByte) && (int32Val >= SByte.MinValue && int32Val <= SByte.MaxValue)) return baseCost + 5;
				else if (conversionType == typeof(Byte) && (int32Val >= Byte.MinValue && int32Val <= Byte.MaxValue)) return baseCost + 6;
				else if (conversionType == typeof(Double)) return baseCost + 9;
				else if (conversionType == typeof(Single)) return baseCost + 10;
				else if (conversionType == typeof(Decimal)) return baseCost + 11;
				else if (conversionType == typeof(Int64)) return -1;
				else if (conversionType == typeof(UInt64)) return -1;
				return -1;
			}
			else if (valueType == typeof(Double))
			{
				double doubleVal = (double)value;

				if (conversionType == typeof(Double)) return baseCost + 0;
				else if (conversionType == typeof(Single) && (doubleVal >= Single.MinValue && doubleVal <= Single.MaxValue)) return baseCost + 1;
				else if (conversionType == typeof(Decimal) /*				 && (doubleVal >= Decimal::MinValue && doubleVal <= Decimal::MaxValue) */) return baseCost + 2;
				else if (conversionType == typeof(Int32) && (doubleVal >= Int32.MinValue && doubleVal <= Int32.MaxValue)) return baseCost + 3;
				else if (conversionType == typeof(UInt32) && (doubleVal >= UInt32.MinValue && doubleVal <= UInt32.MaxValue)) return baseCost + 4;
				else if (conversionType == typeof(Int16) && (doubleVal >= Int16.MinValue && doubleVal <= Int16.MaxValue)) return baseCost + 5;
				else if (conversionType == typeof(UInt16) && (doubleVal >= UInt16.MinValue && doubleVal <= UInt16.MaxValue)) return baseCost + 6;
				else if (conversionType == typeof(Char) && (doubleVal >= Char.MinValue && doubleVal <= Char.MaxValue)) return baseCost + 6;
				else if (conversionType == typeof(SByte) && (doubleVal >= SByte.MinValue && doubleVal <= SByte.MaxValue)) return baseCost + 8;
				else if (conversionType == typeof(Byte) && (doubleVal >= Byte.MinValue && doubleVal <= Byte.MaxValue)) return baseCost + 9;
				else if (conversionType == typeof(Int64)) return -1;
				else if (conversionType == typeof(UInt64)) return -1;
				return -1;
			}
			else if (valueType == typeof(String))
			{
				// String can be converted only to String
				if (conversionType == typeof(String)) return baseCost + 0;
				return -1;
			}
			else
			{
				// No conversion available
				return -1;
			}
		}
	}
}
