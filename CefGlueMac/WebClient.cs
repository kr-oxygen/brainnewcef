﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class WebClient : CefClient
	{
		private readonly WebView webView;
		private readonly BindingHandler bindingHandler;
		private readonly LifeSpanHandler lifeSpanHandler;
		private readonly RequestHandler requestHandler;
		private readonly JSDialogHandler jsDialogHandler;
		private readonly LoadHandler loadHandler;

		public WebClient(WebView core, BindingHandler bindingHandler)
		{
			this.webView = core;
			this.bindingHandler = bindingHandler;
			this.lifeSpanHandler = new LifeSpanHandler(this.webView);
			this.requestHandler = new RequestHandler(this.webView);
			this.jsDialogHandler = new JSDialogHandler(this.webView);
			this.loadHandler = new LoadHandler(this.webView);
			//_displayHandler = new WebDisplayHandler(_core);
		}

		protected override CefLifeSpanHandler GetLifeSpanHandler()
		{
			return lifeSpanHandler;
		}

		protected override CefRequestHandler GetRequestHandler()
		{
			return this.requestHandler;
		}

		protected override CefJSDialogHandler GetJSDialogHandler()
		{
			return this.jsDialogHandler;
		}

		protected override CefLoadHandler GetLoadHandler()
		{
			this.webView.LoadHandlerRequested();

			return this.loadHandler;
		}

		protected override bool OnProcessMessageReceived(CefBrowser browser, CefProcessId sourceProcess, CefProcessMessage message)
		{
			if (message.Name == Constants.ContextCreatedMessageName)
			{
				this.bindingHandler.ContextCreated(browser);

				return true;
			}

			if (message.Name == Constants.ContextReleasedMessageName)
			{
				return true;
			}

			if (message.Name == Constants.ExecuteBoundMethodMessageName)
			{
				var objectName = message.Arguments.GetString(0);
				var methodName = message.Arguments.GetString(1);
				var argumentsList = message.Arguments.GetList(2);

				object result;
				Type resultType;
				string exception;
				this.bindingHandler.ExecuteBoundMethod(browser, objectName, methodName, argumentsList, out result, out resultType, out exception);

				return true;
			}

			return WebView.BrowserMessageRouter.OnProcessMessageReceived(browser, sourceProcess, message);
		}
	}
}
