﻿using System;
using System.Diagnostics;

namespace BrainNewCef
{
	public class TestHandler
	{
		public double GetAge()
		{
			return 22.7;
		}

		public int GetAge(int age)
		{
			Debug.WriteLine("Age is : " + age);

			return age;
		}

		public void GetName(string name)
		{
			Debug.WriteLine("Name is : " + name);
		}
	}
}
