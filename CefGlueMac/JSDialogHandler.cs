﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class JSDialogHandler : CefJSDialogHandler
	{
		private WebView _core;

		public JSDialogHandler(WebView core)
		{
			this._core = core;
		}

		#region implemented abstract members of CefJSDialogHandler

		protected override bool OnJSDialog(CefBrowser browser, string originUrl, CefJSDialogType dialogType, string message_text, string default_prompt_text, CefJSDialogCallback callback, out bool suppress_message)
		{
			suppress_message = false;

			var handled = false;
			var returnValue = true;
			var userInput = string.Empty;

			switch (dialogType)
			{
				case CefJSDialogType.Alert:
					handled = this._core.OnAlert(message_text);
					break;
				case CefJSDialogType.Confirm:
					handled = this._core.OnConfirm(message_text, out returnValue);
					break;
				case CefJSDialogType.Prompt:
					handled = this._core.OnPrompt(message_text, default_prompt_text, out returnValue, out userInput);
					break;
			}

			if (handled)
			{
				callback.Continue(returnValue, userInput);
			}

			return handled;
		}

		protected override bool OnBeforeUnloadDialog(CefBrowser browser, string messageText, bool isReload, CefJSDialogCallback callback)
		{
			return false;
		}

		protected override void OnResetDialogState(CefBrowser browser)
		{

		}

		protected override void OnDialogClosed(CefBrowser browser)
		{

		}

		#endregion }
	}
}