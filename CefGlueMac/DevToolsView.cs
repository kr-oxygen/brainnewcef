﻿using System;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public class DevToolsView : WebView
	{
		private readonly CefBrowser ownerBrowser;

		public DevToolsView(CefBrowserSettings browserSettings, CefBrowser ownerBrowser)
			: base(browserSettings)
		{
			this.ownerBrowser = ownerBrowser;
		}

		public override void ViewDidMoveToSuperview()
		{
			if (this.Superview != null)
			{
				var windowInfo = WebView.GetWindowInfo(this.Frame.Size, this.Handle);

				var parenHost = this.ownerBrowser.GetHost();
				parenHost.ShowDevTools(windowInfo, client, settings, new CefPoint(0, 0));
			}
		}
	}
}
