﻿using System;
namespace BrainNewCef
{
	public class LoadEventArgs : EventArgs
	{
		public string Url { get; private set; }
		public bool IsMain { get; private set; }

		public LoadEventArgs(bool isMain, string url)
		{
			this.IsMain = isMain;
			this.Url = url;
		}
	}
}
