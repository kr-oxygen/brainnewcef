﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace BrainNewCef
{
	public class RequestResourceEventArgs : EventArgs
	{
		private string url;
		private string mimeType;
		private string statusText;
		private HttpStatusCode statusCode;
		private IDictionary<string, string> headers;

		private Stream stream;

		internal string MimeType
		{
			get
			{
				return this.mimeType;
			}
		}

		internal string StatusText
		{
			get
			{
				return this.statusText;
			}
		}

		internal HttpStatusCode StatusCode
		{
			get
			{
				return this.statusCode;
			}
		}

		internal Stream Stream
		{
			get
			{
				return stream;
			}
		}

		internal IDictionary<string, string> Headers
		{
			get
			{
				return this.headers;
			}
		}

		internal bool CanProcess { get; private set; }

		public string Url
		{
			get
			{
				return this.url;
			}
		}

		public RequestResourceEventArgs(string url)
		{
			this.url = url;
		}

		public void RespondWith(Stream stream, string mimeType, string statusText, HttpStatusCode statusCode, IDictionary<string, string> responseHeaders)
		{
			this.stream = stream;
			this.mimeType = mimeType;
			this.statusText = statusText;
			this.statusCode = statusCode;
			this.headers = responseHeaders;

			this.CanProcess = true;
		}
	}
}
