// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace BrainNewCef
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		AppKit.NSView BrowserControl { get; set; }

		[Action ("Button:")]
		partial void Button (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (BrowserControl != null) {
				BrowserControl.Dispose ();
				BrowserControl = null;
			}
		}
	}
}
