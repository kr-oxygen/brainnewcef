﻿using System;
using AppKit;
using Foundation;
using ObjCRuntime;

namespace BrainNewCef
{
	[Register("ClientApplication")]
	public class ClientApplication : NSApplication
	{
		private const string CrAppControlProtocolName = "CrAppControlProtocol";

		private bool isHandlingSendEvent;

		public bool IsHandlingSendEvent
		{
			[Export("isHandlingSendEvent")]
			get
			{
				return this.isHandlingSendEvent;
			}
			[Export("setHandlingSendEvent:")]
			set
			{
				this.isHandlingSendEvent = value;
			}
		}

		public ClientApplication(IntPtr handle) : base(handle)
		{
			var protocolHandle = ObjcHelper.GetProtocol(CrAppControlProtocolName);
			ObjcHelper.AddProtocol(new Class(typeof(ClientApplication)), protocolHandle);
		}

		public override void SendEvent(NSEvent theEvent)
		{
			this.isHandlingSendEvent = true;
			base.SendEvent(theEvent);
			this.isHandlingSendEvent = false;
		}
	}
}
