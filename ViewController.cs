﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using AppKit;
using Foundation;
using Xilium.CefGlue;

namespace BrainNewCef
{
	public partial class ViewController : NSViewController
	{
		WebView webBrowser;

		public const string LocalUrlPrefix = "local://simulator/";

		public ViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			//InitializeWebBrowser();

			// Do any additional setup after loading the view.
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
		}

		partial void Button(NSObject sender)
		{
			InitializeWebBrowser();

			while (true)
			{
				CefRuntime.DoMessageLoopWork();
			}

		}

		private bool _shouldStopMessageLoop;

		private void InitializeWebBrowser()
		{
			var homeUrl = "https://bitbucket.org/xilium/xilium.cefglue/wiki/Home";

			var browserSettings = new CefBrowserSettings();
			this.webBrowser = new WebView(homeUrl, browserSettings);
			//			this.webBrowser.RequestResource += this.HandleProcessRequest;
			//			this.webBrowser.DevToolsShown += this.OnDevToolsShown;

			this.webBrowser.SetFrameSize(this.BrowserControl.Frame.Size);
			this.BrowserControl.AddSubview(this.webBrowser);
			this.webBrowser.RegisterJsObject("Test_Handler", new TestHandler());
			this.webBrowser.RequestResource += this.HandleProcessRequest;

			this.webBrowser.JSAlert += this.OnJSAlert;
			this.webBrowser.JSConfirm += this.OnJSConfirm;
			this.webBrowser.JSPrompt += this.OnJSPrompt;
			this.webBrowser.Created += (sender, e) =>
			{
				Console.WriteLine("Set stop message loop");
				_shouldStopMessageLoop = true;
			};
			this.webBrowser.DevToolsShown += this.OnDevToolsShown;
		}

		private void HandleProcessRequest(object sender, RequestResourceEventArgs e)
		{
			if (e.Url.StartsWith(LocalUrlPrefix))
			{
				var localUrl = e.Url.Substring(LocalUrlPrefix.Length);
				if (File.Exists("/" + localUrl))
				{
					var mimeType = MimeTypeHelper.GetMimeType(Path.GetExtension(localUrl));
					e.RespondWith(File.OpenRead(localUrl), mimeType, localUrl, HttpStatusCode.OK, new Dictionary<string, string>() { { "Access-Control-Allow-Origin", "*" } });
				}
			}
		}

		private void OnHideDevToolsActivated(object sender, EventArgs e)
		{
			this.webBrowser.DevToolsShown -= this.OnDevToolsShown;

			//foreach (var subView in this.DevToolsControl.Subviews)
			//{
			//	subView.RemoveFromSuperview();
			//	subView.Dispose();
			//}

			this.webBrowser.CloseDevTools();
		}

		private void OnZoomChanged(object sender, EventArgs e)
		{
			//var scaleFactor = this.ZoomStepper.DoubleValue;
			//if (!double.IsNaN(scaleFactor))
			//{
			//	// TODO: Perform zooming
			//	this.webBrowser.SetScaleFactor(scaleFactor);
			//	this.ZoomTextField.DoubleValue = scaleFactor;
			//}
		}

		private void OnGoButtonActivated(object sender, EventArgs e)
		{
			//var content = this.NavigationField.StringValue;
			//if (!string.IsNullOrEmpty(content))
			//{
			//	this.webBrowser.Navigate(content);
			//}
		}

		private void OnShowDevToolsActivated(object sender, EventArgs e)
		{
			this.webBrowser.DevToolsShown += this.OnDevToolsShown;
			this.webBrowser.ShowDevTools();
		}

		private void OnExecuteAlertButtonActivated(object sender, EventArgs e)
		{
			this.webBrowser.ExecuteScript("alert('Hello')");
		}

		private void OnJSAlert(object sender, JSDialogEventArgs e)
		{
			//e.Handled = true;
		}

		private void OnJSConfirm(object sender, JSDialogEventArgs e)
		{
			//            e.ReturnValue = true;
			//            e.Handled = true;
		}

		private void OnJSPrompt(object sender, JSDialogEventArgs e)
		{
			//            e.ReturnValue = true;
			//            e.UserInput = "User input";
			//            e.Handled = true;
		}

		private void OnRefreshButtonActivated(object sender, EventArgs e)
		{
			this.webBrowser.Reload();
		}

		private void OnDevToolsShown(object sender, DevToolsShownEventArgs e)
		{
			//var content = e.View;
			//content.SetFrameSize(this.DevToolsControl.Frame.Size);
			//this.DevToolsControl.AddSubview(content);
		}

		public override NSObject RepresentedObject
		{
			get
			{
				return base.RepresentedObject;
			}
			set
			{
				base.RepresentedObject = value;
				// Update the view, if already loaded.
			}
		}
	}
}
