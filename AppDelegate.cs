﻿using AppKit;
using Foundation;
using Xilium.CefGlue;

namespace BrainNewCef
{
	[Register("AppDelegate")]
	public class AppDelegate : NSApplicationDelegate
	{
		public AppDelegate()
		{
			this.Initialize();
		}

		public override void DidFinishLaunching(NSNotification notification)
		{
			// Insert code here to initialize your application


			//CefRuntime.Shutdown();
		}

		public override void WillTerminate(NSNotification notification)
		{
			// Insert code here to tear down your application
			CefBootstrapper.Terminate();
		}

		private void Initialize()
		{
			var settings = new CefSettings();

			settings.LogSeverity = CefLogSeverity.Verbose;
			settings.SingleProcess = false;
			settings.RemoteDebuggingPort = 20480;


			CefBootstrapper.Initialize(settings);

			//viewController = new MainWindowController();
			//viewController..Window.MakeKeyAndOrderFront(this);
		}

		public override NSApplicationTerminateReply ApplicationShouldTerminate(NSApplication sender)
		{
			return NSApplicationTerminateReply.Now;
		}
	}
}
